#include<PubSubClient.h>
#include <ArduinoJson.h>
#include <EnvironmentCalculations.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <Wire.h>
#include <time.h>

#include "DHT.h"
#define DHTPIN 4     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);

const char* deviceId = "FarmBuddy_Sensor_Node01"; // set device ID here
const char * mqtt_broker = "192.168.100.5";
//const char * mqtt_broker = "broker.hivemq.com";
const char * mqtt_topic = "juliesundar/codettes/01"; // CHANGE SensorID here!
const char * mqtt_User = "";
const char * mqtt_Password = "";

//AP Variables
const char* ssid_ap = "FarmBuddy-Sensor";
const char* password_ap = "12345678";

WiFiClient espClient;
PubSubClient client(espClient);

float temp, hum, heatIndexF, f ;

char output[1023];

/*Put your SSID & Password*/
const char * ssid = "HUAWEI-2.4G-bF2k";//"Team09"; //"Virus"; //"Team09"; //"Virus"; //"HUAWEI-3FDA";//"HUAWEI-3FDA"; // Enter SSID here
const char * password = "DuuBnx2S"; // "H@ckTe@m)("; //"RedEyeJedi44"; //"H@ckTe@m)(";  //"YB7DJY63ARE"; //"RedEyeJedi44"; //"YB7DJY63ARE"; // //Enter Password here

AsyncWebServer server(80);

void setup() {

  Serial.begin(115200);
  // Initialize SPIFFS
  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  //config AP Mode
  Serial.println("\n[*] Creating AP");
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid_ap, password_ap);
  Serial.print("[+] AP Created with IP Gateway ");
  Serial.println(WiFi.softAPIP());

  //config Station Mode
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());
  client.setServer(mqtt_broker, 1883);
  if (initMqtt()) {
    client.publish("juliesundar/codettes/01", "Hello from ESP32");
  }

  //server.serveStatic("/", SPIFFS, "/data/");

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(temp).c_str());
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(hum).c_str());
  });
  server.on("/heatIndex", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(heatIndexF).c_str());
  });
  server.on("/js/highcharts.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/highcharts.js", "text/javascript");
  });

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/home.html");
  });

  // Start server
  server.begin();

  Serial.println("HTTP server started");
  Wire.begin();
  dht.begin();
  Serial.println("Connecting to ");
  Serial.println(ssid);

}

unsigned long lastMqttTime = millis();
const unsigned long Mqtt_INTERVAL_MS = 2000;

void loop() {

  client.loop();

  // Send regular mqtt data
  if ((millis() - lastMqttTime) > Mqtt_INTERVAL_MS) {
    if (client.connected())
    {
      GetWeatherData();
      handle_MqttData();
      lastMqttTime = millis();
    } else
    {
      initMqtt();
    }

  }
}

bool initMqtt()
{
  if (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP32Client", mqtt_User, mqtt_Password )) {

      Serial.println("connected");
      return true;

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
      return false;
    }
  }
}

bool initBMP()
{
  dht.begin();
}


void GetWeatherData() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  hum = dht.readHumidity();
  // Read temperature as Celsius (the default)
  temp = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(hum) || isnan(temp) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float heatIndexF = dht.computeHeatIndex(f, hum);
  // Compute heat index in Celsius (isFahreheit = false)
  float heatIndexC = dht.computeHeatIndex(temp, hum, false);

}

void handle_MqttData() {

  StaticJsonDocument<1023> doc;
  doc["deviceId"] = deviceId;
  doc["temp"] = temp;
  doc["humid"] = hum;
  doc["heatIndex"] = heatIndexF;
  

  char out[128];
  int b = serializeJson(doc, out);
  serializeJson(doc, Serial);
  Serial.print("bytes ->  ");
  Serial.print(b, DEC);
  // add if delivered or not
  client.publish((char*)mqtt_topic, out) ? Serial.println(" -> delivered") : Serial.println(" -> failed");
  delay(4000);
}
