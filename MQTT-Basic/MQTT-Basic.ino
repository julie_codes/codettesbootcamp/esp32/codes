#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>

// ### WiFi stuff ###
// Client credits
const char* ssid = "HUAWEI-2.4G-bF2k";
const char* password = "DuuBnx2S";

// AccessPoint info
const char * hostName = "HACKTEAM-AP";

void initWiFi() {
  Serial.print("Connecting WiFi Dual mode:");
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.printf(".");
    WiFi.disconnect(false);
    delay(1000);
    WiFi.begin(ssid, password);
  }

  WiFi.softAP(hostName);
  Serial.print("Access Point SSID: ");
  Serial.print(hostName);
  Serial.print(" / IP : ");
  Serial.println(WiFi.softAPIP());
  Serial.print("WiFi Client IP: ");
  Serial.println(WiFi.localIP());
}


// ### MQTT stuff ###
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
char *mqttServer = "broker.hivemq.com";
int mqttPort = 1883;

void initMQTT() {
  mqttClient.setServer(mqttServer, mqttPort);
  // set the callback function
  mqttClient.setCallback(MQTTcallback);
}

void MQTTcallback(char* topic, byte* payload, unsigned int length) {
  // What to do with inbound MQTT message (for now just Serial print)
  String topicStr = topic; // casts the Char array into a String
  String message = (char *)payload; // casts Byte Array into String
  Serial.print("Callback ~ ");
  Serial.print("Topic : ");
  Serial.print(topic);
  Serial.print(" / Message: ");
  Serial.println(message);
  // handler for each topic/type ...
  if(topicStr == "/hackomation/theo/io/2/set"){
    Serial.print("Updating I/O ");
    Serial.print("2");
    digitalWrite(2, !digitalRead(2)); // for now just toggle
  }
}

void reconnectMQTT() {
  Serial.println("Connecting to MQTT Broker...");
  while (!mqttClient.connected()) {
    Serial.println("Reconnecting to MQTT Broker..");
    String clientId = "ESP32theo-";
    clientId += String(random(0xffff), HEX);

    if (mqttClient.connect(clientId.c_str())) {
      Serial.println("Connected.");
      // subscribe to required topics
      mqttClient.subscribe("/hackomation/theo/io/#");   // eg. listens only to IO commands io/x/state or io/x/set
      mqttClient.subscribe("/hackomation/theo/event/#"); // system & environmental /event/sun/rise or set
    }
  }
}

unsigned long updLast = millis();
unsigned long updTimer = 10000;

void updateState() {
  if (millis() - updLast > updTimer) {
    // Get sensor data here
    float temperature = 22.22; // bme.readTemperature();
    float humidity = 99.99; //bme.readHumidity();
    // Publishing data through MQTT
    mqttClient.publish("/hackomation/theo/temperature", String(temperature).c_str());
    mqttClient.publish("/hackomation/theo/humidity", String(humidity).c_str());
    updLast = millis();
    // debug print
    Serial.print("MQTT sent: /hackomation/theo/temperature ");
    Serial.println(String(temperature).c_str());
    Serial.print("MQTT sent: /hackomation/theo/humidity ");
    Serial.println(String(humidity).c_str());
  }
}

void setup() {
  Serial.begin(115200);
  initWiFi();
  initMQTT();

  // Setup IOs
  pinMode(2,OUTPUT);
}

unsigned long last_time = 0;
char *data;

void loop() {
  if (!mqttClient.connected())
    reconnectMQTT();

  mqttClient.loop();
  
  updateState(); // will update state based on updTimer
}
