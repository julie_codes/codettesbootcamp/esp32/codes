#include <Regexp.h>

/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com  https://randomnerdtutorials.com/esp32-mqtt-publish-subscribe-arduino-ide/
  //juliesundar/codettes/01
  juliesundar/codettes/pump

  Note it doesnt matter what topic you are subscribing to as long as the broker is writing to that one
  Nodejs Dont Matter I tested it with MQTT LENS
*********/

#include <WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <ArduinoJson.h>

// Replace the next variables with your SSID/Password combination
//const char* ssid = "Team09";
//const char* password = "H@ckTe@m)(";

/*Put your SSID & Password*/
const char * ssid = "HUAWEI-2.4G-bF2k";//"Team09"; //"Virus"; //"Team09"; //"Virus"; //"HUAWEI-3FDA";//"HUAWEI-3FDA"; // Enter SSID here
const char * password = "DuuBnx2S"; // "H@ckTe@m)("; //"RedEyeJedi44"; //"H@ckTe@m)(";  //"YB7DJY63ARE"; //"RedEyeJedi44"; //"YB7DJY63ARE"; // //Enter Password here

// Add your MQTT Broker IP address, example:
const char* mqtt_server = "192.168.100.5";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;


// LED Pin
const int ledPin = 2;

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  pinMode(ledPin, OUTPUT);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

StaticJsonDocument<1023> doc;

void callback(char* topic, byte* message, unsigned int length) {
  //Serial.print("Message arrived on topic: ");
  //Serial.print(topic);
  //Serial.print(". Message: ");
  String messageStr = (char *)message;
  //Serial.println(messageStr);

  //  for (int i = 0; i < length; i++) {
  //    Serial.print((char)message[i]);
  //    //String str = String(message[i]);   // Convert the char to a String
  //    //String sub_S = str.substring(0,2);
  //    //Serial.print(str);
  //    messageStr += (char)message[i];
  //  }
  //  Serial.println();

  // If a message is received on the topic esp32/output, you check if the message is either "on" or "off".
  // Changes the output state according to the message
  DeserializationError JSONerror = deserializeJson(doc, message); // Returns TRUE if JSON was detected in the input

  if (!JSONerror) {
    if (doc["temp"] > 20) {
      serializeJsonPretty(doc, Serial);
      //Serial.println(doc["temp"]);

      digitalWrite(ledPin, !digitalRead(ledPin));
    }
  } else {
    // detect the rest
    Serial.println("JSON Error or NOT JSON data");
    if (String(topic) == "juliesundar/codettes/01") {
      // Serial.print(topic);
      // Serial.print("Changing output to ");
      if (messageStr == "on") {
        Serial.println("on");
        digitalWrite(ledPin, HIGH);
      }
      else if (messageStr == "off") {
        Serial.println("off");
        digitalWrite(ledPin, LOW);
      }
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Subscribe
      //client.subscribe("juliesundar/codettes/01");
      client.subscribe("juliesundar/codettes/01");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis();
  if (now - lastMsg > 5000) {

    lastMsg = now;

  }
}
