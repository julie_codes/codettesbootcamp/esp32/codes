#include <ArduinoJson.h>

const char *soft_ap_ssid = "VisionAP";
const char *soft_ap_password = "12345678";

const char* wifi_network_ssid = "telew_f2a";     //"Team09";
const char* wifi_network_password = "aa003234";  //"H@ckTe@m)(";

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
  
void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  
  if(type == WS_EVT_CONNECT){
  
    Serial.println("Websocket client connection received");
     
  } else if(type == WS_EVT_DISCONNECT){
 
    Serial.println("Client disconnected");
  
  }
}

void setup() {
  // put your setup code here, to run once:
  WiFi.mode(WIFI_MODE_APSTA);

  WiFi.softAP(soft_ap_ssid, soft_ap_password);
  Serial.begin(115200);
  Serial.print("ESP32 IP on the WiFi network: ");
  Serial.println(WiFi.localIP());

  // Initialize SPIFFS
  if (!SPIFFS.begin(true))
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  server.on("/", HTTP_GET, onIndexRequest);

  server.serveStatic("/", SPIFFS, "/");

    events.onConnect([](AsyncEventSourceClient * client) {
    if (client->lastId()) {
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
    }
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello!", NULL, millis(), 10000);
  });
  server.addHandler(&events);
  server.begin();

  webSocket.begin();
  webSocket.onEvent(onWebSocketEvent);

    ws.onEvent(onWsEvent);
  server.addHandler(&ws);
  
  server.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
webSocket.loop();
webSocket.broadcastTXT(let);
StaticJsonBuffer<300> JSONbuffer;
          JsonObject& JSONencoder = JSONbuffer.createObject();
          JSONencoder["temp"] = temp;
          JSONencoder["hum"] = hum;
      
          char JSONmessageBuffer[100];
          JSONencoder.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
          webSocket.broadcastTXT(JSONmessageBuffer);
          //client.publish("vision", JSONmessageBuffer);
}
