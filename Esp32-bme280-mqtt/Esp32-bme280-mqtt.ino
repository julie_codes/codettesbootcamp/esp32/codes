
#include <SPI.h>
#include<PubSubClient.h>
#include <ArduinoJson.h>
#include <EnvironmentCalculations.h>
#include <BME280I2C.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
//#include <WebServer.h>
#include <Wire.h>

static const unsigned char PROGMEM logo_bmp[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000
};

const char * mqtt_broker = "192.168.100.5";
//const char * mqtt_broker = "broker.hivemq.com";
const char * mqtt_topic = "juliesundar/codettes/01"; // CHANGE SensorID here!
const char * mqtt_User = "";
const char * mqtt_Password = "";

// Assumed environmental values:
float referencePressure = 1018.6; // hPa local QFF (official meteor-station reading)
float outdoorTemp = 28; // °C  measured local outdoor temp.
float barometerAltitude = 1650.3; // meters ... map readings + barometer position

BME280I2C::Settings settings(
  BME280::OSR_X1,
  BME280::OSR_X1,
  BME280::OSR_X1,
  BME280::Mode_Forced,
  BME280::StandbyTime_1000ms,
  BME280::Filter_16,
  BME280::SpiEnable_False,
  BME280I2C::I2CAddr_0x76
);

BME280I2C bme(settings);

WiFiClient espClient;
PubSubClient client(espClient);

/*
   getLocalTimeNTP: Get Epoch Time Stamp from NTP server
*/
unsigned long getLocalTimeNTP() {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime( & timeinfo)) {
    Serial.println("Failed to obtain time");
    return 0;
  }
  time( & now);
  return now;
}


float temp, hum, pres, altitude , dewPoint, seaLevel, absHum, heatIndex ;

char output[1023];

/*Put your SSID & Password*/
const char * ssid = "HUAWEI-2.4G-bF2k";//"Team09"; //"Virus"; //"Team09"; //"Virus"; //"HUAWEI-3FDA";//"HUAWEI-3FDA"; // Enter SSID here
const char * password = "DuuBnx2S"; // "H@ckTe@m)("; //"RedEyeJedi44"; //"H@ckTe@m)(";  //"YB7DJY63ARE"; //"RedEyeJedi44"; //"YB7DJY63ARE"; // //Enter Password here

//WebServer server(80);
AsyncWebServer server(80);

void setup() {
  Serial.begin(115200);

  // Initialize SPIFFS
  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());
  client.setServer(mqtt_broker, 1883);
  if (initMqtt()) {
    client.publish("juliesundar/codettes/01", "Hello from ESP32");
  }

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(temp).c_str());
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(hum).c_str());
  });
  server.on("/pressure", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(pres).c_str());
  });
  server.on("/altitude", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(altitude).c_str());
  });
  server.on("/seaLevel", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(seaLevel).c_str());
  });
  server.on("/absHum", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(absHum).c_str());
  });
  server.on("/heatIndex", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(heatIndex).c_str());
  });

  // Start server
  server.begin();

  Serial.println("HTTP server started");

  Wire.begin();
  initBMP();

  Serial.println("Connecting to ");
  Serial.println(ssid);

}

unsigned long lastMqttTime = millis();
const unsigned long Mqtt_INTERVAL_MS = 2000;

void loop() {

  client.loop();

  // Send regular mqtt data
  if ((millis() - lastMqttTime) > Mqtt_INTERVAL_MS) {
    if (client.connected())
    {
      GetWeatherData();
      handle_MqttData();
      lastMqttTime = millis();
    } else
    {
      initMqtt();
    }


  }
}

bool initMqtt()
{
  if (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP32Client", mqtt_User, mqtt_Password )) {

      Serial.println("connected");
      return true;

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
      return false;
    }
  }
}

bool initBMP()
{
  while (!bme.begin()) {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }

  switch (bme.chipModel()) {
    case BME280::ChipModel_BME280:
      Serial.println("Found BME280 sensor! Success.");
      break;
    case BME280::ChipModel_BMP280:
      Serial.println("Found BMP280 sensor! No Humidity available.");
      break;
    default:
      Serial.println("Found UNKNOWN sensor! Error!");
  }
  Serial.print("Assumed outdoor temperature: ");
  Serial.print(outdoorTemp);
  Serial.print("°C\nAssumed reduced sea level Pressure: ");
  Serial.print(referencePressure);
  Serial.print("hPa\nAssumed barometer altitude: ");
  Serial.print(barometerAltitude);
  Serial.println("m\n***************************************");
}


void GetWeatherData() {
  // Serial.print("GETTTTING WEATHER DATA");
  //float temp(NAN), hum(NAN), pres(NAN);
  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  BME280::PresUnit presUnit(BME280::PresUnit_hPa);
  bme.read(pres, temp, hum, tempUnit, presUnit);

  EnvironmentCalculations::AltitudeUnit envAltUnit  =  EnvironmentCalculations::AltitudeUnit_Meters;
  EnvironmentCalculations::TempUnit     envTempUnit =  EnvironmentCalculations::TempUnit_Celsius;
  //Serial.print("ENVTEMPUNIT");

  /// To get correct local altitude/height (QNE) the reference Pressure
  ///    should be taken from meteorologic messages (QNH or QFF)

  altitude = EnvironmentCalculations::Altitude(pres, envAltUnit, referencePressure, outdoorTemp, envTempUnit);
  dewPoint = EnvironmentCalculations::DewPoint(temp, hum, envTempUnit);

  /// To get correct seaLevel pressure (QNH, QFF)
  ///    the altitude value should be independent on measured pressure.
  /// It is necessary to use fixed altitude point e.g. the altitude of barometer read in a map
  seaLevel = EnvironmentCalculations::EquivalentSeaLevelPressure(barometerAltitude, temp, pres, envAltUnit, envTempUnit);
  absHum = EnvironmentCalculations::AbsoluteHumidity(temp, hum, envTempUnit);
  heatIndex = EnvironmentCalculations::HeatIndex(temp, hum, envTempUnit);

}

void handle_MqttData() {

  StaticJsonDocument<1023> doc;
  //JsonObject createNestedObject() const;

  doc["temp"] = temp;
  doc["humid"] = hum;
  doc["pres"] = pres;
  doc["altitude"] = altitude;
  doc["dewPoint"] = dewPoint;
  doc["seaLevel"] = seaLevel;
  doc["absHum"] = absHum;
  doc["heatIndex"] = heatIndex;

  char out[128];
  int b = serializeJson(doc, out);
  serializeJson(doc, Serial);
  Serial.print("bytes ->  ");
  Serial.print(b, DEC);
  // add if delivered or not
  client.publish((char*)mqtt_topic, out) ? Serial.println(" -> delivered") : Serial.println(" -> failed");
}
